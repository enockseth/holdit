var wms_layers = [];

        var lyr_OCANima_0 = new ol.layer.Tile({
            'title': 'OCA Nima',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: '<a href=""></a>',
                url: 'https://tiles.openaerialmap.org/5d2d0ab4f416f40006cffcc1/0/5d2d0ab4f416f40006cffcc2/{z}/{x}/{y}'
            })
        });var format_20190823_Nima_Buildings_1 = new ol.format.GeoJSON();
var features_20190823_Nima_Buildings_1 = format_20190823_Nima_Buildings_1.readFeatures(json_20190823_Nima_Buildings_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_20190823_Nima_Buildings_1 = new ol.source.Vector({
    attributions: '<a href=""></a>',
});
jsonSource_20190823_Nima_Buildings_1.addFeatures(features_20190823_Nima_Buildings_1);var lyr_20190823_Nima_Buildings_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_20190823_Nima_Buildings_1, 
                style: style_20190823_Nima_Buildings_1,
                title: '<img src="styles/legend/20190823_Nima_Buildings_1.png" /> 20190823_Nima_Buildings'
            });var format_Nima_GARID_2 = new ol.format.GeoJSON();
var features_Nima_GARID_2 = format_Nima_GARID_2.readFeatures(json_Nima_GARID_2, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Nima_GARID_2 = new ol.source.Vector({
    attributions: '<a href=""></a>',
});
jsonSource_Nima_GARID_2.addFeatures(features_Nima_GARID_2);var lyr_Nima_GARID_2 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Nima_GARID_2, 
                style: style_Nima_GARID_2,
                title: '<img src="styles/legend/Nima_GARID_2.png" /> Nima_GARID'
            });

lyr_OCANima_0.setVisible(true);lyr_20190823_Nima_Buildings_1.setVisible(true);lyr_Nima_GARID_2.setVisible(true);
var layersList = [lyr_OCANima_0,lyr_20190823_Nima_Buildings_1,lyr_Nima_GARID_2];
lyr_20190823_Nima_Buildings_1.set('fieldAliases', {'building': 'building', 'type': 'type', 'source': 'source', 'building:levels': 'building:levels', 'alt_name': 'alt_name', 'amenity': 'amenity', 'name': 'name', 'religion': 'religion', 'roof:colour': 'roof:colour', 'shop': 'shop', 'layer': 'layer', 'natural': 'natural', 'office': 'office', 'surface': 'surface', 'fixme': 'fixme', 'addr:city': 'addr:city', 'addr:housenumber': 'addr:housenumber', 'addr:street': 'addr:street', 'contact:email': 'contact:email', 'contact:phone': 'contact:phone', 'country': 'country', 'diplomatic': 'diplomatic', 'name:cs': 'name:cs', 'name:de': 'name:de', 'name:en': 'name:en', 'name:it': 'name:it', 'opening_hours': 'opening_hours', 'website': 'website', 'addr:country': 'addr:country', 'addr:postcode': 'addr:postcode', 'description': 'description', 'wikidata': 'wikidata', 'wikipedia': 'wikipedia', });
lyr_Nima_GARID_2.set('fieldAliases', {'fid': 'fid', 'landuse': 'landuse', 'name': 'name', });
lyr_20190823_Nima_Buildings_1.set('fieldImages', {'building': '', 'type': '', 'source': '', 'building:levels': '', 'alt_name': '', 'amenity': '', 'name': '', 'religion': '', 'roof:colour': '', 'shop': '', 'layer': '', 'natural': '', 'office': '', 'surface': '', 'fixme': '', 'addr:city': '', 'addr:housenumber': '', 'addr:street': '', 'contact:email': '', 'contact:phone': '', 'country': '', 'diplomatic': '', 'name:cs': '', 'name:de': '', 'name:en': '', 'name:it': '', 'opening_hours': '', 'website': '', 'addr:country': '', 'addr:postcode': '', 'description': '', 'wikidata': '', 'wikipedia': '', });
lyr_Nima_GARID_2.set('fieldImages', {'fid': '', 'landuse': 'TextEdit', 'name': 'TextEdit', });
lyr_20190823_Nima_Buildings_1.set('fieldLabels', {'building': 'no label', 'type': 'no label', 'source': 'no label', 'building:levels': 'no label', 'alt_name': 'no label', 'amenity': 'no label', 'name': 'no label', 'religion': 'no label', 'roof:colour': 'no label', 'shop': 'no label', 'layer': 'no label', 'natural': 'no label', 'office': 'no label', 'surface': 'no label', 'fixme': 'no label', 'addr:city': 'no label', 'addr:housenumber': 'no label', 'addr:street': 'no label', 'contact:email': 'no label', 'contact:phone': 'no label', 'country': 'no label', 'diplomatic': 'no label', 'name:cs': 'no label', 'name:de': 'no label', 'name:en': 'no label', 'name:it': 'no label', 'opening_hours': 'no label', 'website': 'no label', 'addr:country': 'no label', 'addr:postcode': 'no label', 'description': 'no label', 'wikidata': 'no label', 'wikipedia': 'no label', });
lyr_Nima_GARID_2.set('fieldLabels', {'fid': 'no label', 'landuse': 'no label', 'name': 'no label', });
lyr_Nima_GARID_2.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});